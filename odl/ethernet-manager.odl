#include "mod_sahtrace.odl";
#include "mod_apparmor.odl";
#include "global_amxb_timeouts.odl";

%config {
    name = "ethernet-manager";
    mod_name = "mod_nm";
    import-dbg = false;
    dm-eventing-enabled = true;
    dm-events-before-start = true;

    // prefix for vendor specific objects and/or parameters
    prefix_ = "X_PRPL-COM_";

    odl = {
        dm-save = true,
        dm-save-on-changed = true,
        dm-save-delay = 1000,
        dm-defaults = "${name}_defaults/",
        mac-defaults = "${name}_default_mac_addresses.odl",
        directory = "${storage-path}/odl"
    };

    pcm_svc_config = {
        "Objects" = "Ethernet"
    };

    // main files
    definition_file = "${name}_definition.odl";

    // Other options
    // SAHTRACE
    sahtrace = {
        type = "syslog",
        level = "${default_log_level}"
    };

    trace-zones = {
        ethernet = "${default_trace_zone_level}",
        actions = "${default_trace_zone_level}",
        events = "${default_trace_zone_level}",
        utils = "${default_trace_zone_level}",
        nm_populate = "${default_trace_zone_level}",
        mac = "${default_trace_zone_level}",
        rpc = "${default_trace_zone_level}",
        eth-utils = "${default_trace_zone_level}"
    };

    ethernet-manager = {
        mod-dir = "/usr/lib/amx/${name}/modules",
        external-mod-dir = "/usr/lib/amx/modules"
    };

    NetModel = "nm_intfs,nm_links,nm_vlan";
    //section for Ethernet Interfaces
    nm_intfs = {
        InstancePath = "Ethernet.Interface.",
        Tags = "eth_intf netdev",
        Prefix = "ethIntf-"
    };

    //section for Ethernet Links
    nm_links = {
        InstancePath = "Ethernet.Link.",
        Tags = "eth_link",
        Prefix = "ethLink-"
    };

    //section for Ethernet VLANTermination
    nm_vlan = {
        InstancePath = "Ethernet.VLANTermination.",
        Tags = "vlan netdev",
        Prefix = "vlan-"
    };

}

requires "NetDev.";
requires "NetModel.";
import "mod-dmstats.so";
import "mod-dmext.so";
import "mod-netmodel.so" as "${mod_name}";
import "${name}.so" as "${name}";

#include "ethernet-manager_caps.odl";
#include "ethernet-manager_config.odl";
include "${definition_file}";

%define {
    entry-point mod_nm.mod_netmodel_main;
    entry-point ethernet-manager.ethernet_manager_main;
}

#include "mod_pcm_svc.odl";
