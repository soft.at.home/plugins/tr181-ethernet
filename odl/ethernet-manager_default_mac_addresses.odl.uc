%populate {
{% let i=1 %}
{% for (let Itf in BD.Interfaces) : if (Itf.Upstream == "true" || Itf.DefaultUpstream == "true") : %}{% i++ %}
    object "Ethernet.Link.{{i}}" {
        parameter MACAddress = "$(BASEMACADDRESS)";
    }
{% endif; endfor; %}
{% let i = 0 %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
    object Ethernet.Link.bridge_{{lc(Bridge)}} {
        parameter MACAddress = "$(BASEMACADDRESS_PLUS_{{i}})";
    }
{% endfor; %}
}
