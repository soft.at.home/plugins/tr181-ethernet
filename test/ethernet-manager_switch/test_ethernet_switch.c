/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "ethernet.h"
#include "test_ethernet_switch.h"
#include "common_functions.h"

#define ETH_INTF_TEST "Ethernet.Interface.test-intf."

int test_ethernet_switch_setup(void** state) {
    const char* odl_defs = "../../odl/ethernet-manager_definition.odl";
    const char* odl_defaults = "ethernet-manager_defaults.odl";
    const char* odl_mock_netdev = "../common/mock_netdev_dm.odl";
    const char* odl_config = "../common/mock.odl";
    const char* odl_mock_netmodel = "../common/mock_netmodel.odl";
    const char* odl_mock_switch = "mock_switch-manager.odl";

    amxut_bus_setup(state);

    resolver_add_all_functions(amxut_bus_parser());

    amxut_dm_load_odl(odl_mock_netdev);
    amxut_dm_load_odl(odl_mock_netmodel);
    amxut_dm_load_odl(odl_config);
    amxut_dm_load_odl(odl_defs);
    amxut_dm_load_odl(odl_defaults);
    amxut_dm_load_odl(odl_mock_switch);

    assert_int_equal(_ethernet_manager_main(AMXO_START, amxut_bus_dm(), amxut_bus_parser()), 0);

    amxut_bus_handle_events();

    return 0;
}

int test_ethernet_switch_teardown(void** state) {
    _ethernet_manager_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser());

    amxo_resolver_import_close_all();

    amxut_bus_teardown(state);

    return 0;
}

void test_can_add_interface_with_switch_port(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "SwitchPort", "Switch.Port.1.");
    amxd_trans_set_value(cstring_t, &trans, "Alias", "test-intf");
    amxd_trans_set_value(cstring_t, &trans, "Name", "test-intf");
    amxd_trans_set_value(cstring_t, &trans, "DefaultController", "mod-eth-dummy");
    amxd_trans_set_value(cstring_t, &trans, "RMONStatsController", "mod-rmonstats-dummy");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxc_var_dump(&trans.retvals, 1);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
}

void test_can_sync_to_switch(UNUSED void** state) {
    amxc_var_t ret;
    amxd_trans_t trans;

    amxc_var_init(&ret);

    // Interface is initially disabled, so Port is also disabled
    amxb_get(amxut_bus_ctx(), "Switch.Port.1.Enable", 0, &ret, 5);
    assert_false(GETP_BOOL(&ret, "0.0.Enable"));

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, ETH_INTF_TEST);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    // Port is kept in sync with Interface
    amxb_get(amxut_bus_ctx(), "Switch.Port.1.Enable", 1, &ret, 5);
    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    amxc_var_clean(&ret);
}

void test_can_sync_led_to_switch(UNUSED void** state) {
    amxo_parser_t* parser = amxut_bus_parser();
    const char* prefix = GET_CHAR(amxo_parser_get_config(parser, "prefix_"), NULL);
    amxc_var_t ret;
    amxd_trans_t trans;
    amxc_string_t led_obj;

    amxc_var_init(&ret);
    amxc_string_init(&led_obj, 0);
    amxc_string_setf(&led_obj, "%s%sLED.", ETH_INTF_TEST, prefix);

    // Interface.LED is initially enabled, so Port.LED is also enabled
    amxb_get(amxut_bus_ctx(), "Switch.Port.1.LED.Enable", 0, &ret, 5);
    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, amxc_string_get(&led_obj, 0));
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    // Port is kept in sync with Interface
    amxb_get(amxut_bus_ctx(), "Switch.Port.1.LED.Enable", 0, &ret, 5);
    assert_false(GETP_BOOL(&ret, "0.0.Enable"));

    amxc_string_clean(&led_obj);
    amxc_var_clean(&ret);
}

void test_can_sync_from_switch(UNUSED void** state) {
    amxc_var_t ret;
    amxd_trans_t trans;

    amxc_var_init(&ret);

    // Initial Status is Down
    amxb_get(amxut_bus_ctx(), ETH_INTF_TEST "Status", 0, &ret, 5);
    assert_string_equal(GETP_CHAR(&ret, "0.0.Status"), "Down");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Switch.Port.1.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "PortStatus", "Up");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    // Interface is kept in sync with Port
    amxb_get(amxut_bus_ctx(), ETH_INTF_TEST "Status", 0, &ret, 5);
    assert_string_equal(GETP_CHAR(&ret, "0.0.Status"), "Up");

    amxc_var_clean(&ret);
}

void test_can_get_stats_from_switch(UNUSED void** state) {
    amxc_var_t ret;
    amxd_trans_t trans;
    uint64_t value = 123;

    amxc_var_init(&ret);

    // First set some random stats values
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Switch.Port.1.Stats.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(uint64_t, &trans, "BytesSent", value);
    amxd_trans_set_value(uint64_t, &trans, "BytesReceived", value);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    // Now get Ethernet stats, which will in turn get Port stats
    amxb_get(amxut_bus_ctx(), ETH_INTF_TEST "Stats.", 0, &ret, 5);
    assert_int_equal(GETP_UINT32(&ret, "0.0.BytesSent"), value);
    assert_int_equal(GETP_UINT32(&ret, "0.0.BytesReceived"), value);
    assert_int_equal(GETP_UINT32(&ret, "0.0.UnicastPacketsSent"), 0);

    amxc_var_clean(&ret);
}

void test_can_get_rmon_stats_from_switch(UNUSED void** state) {
    amxc_var_t ret;
    amxd_trans_t trans;
    uint32_t value = 123;

    amxc_var_init(&ret);

    // First set some random stats values
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Switch.Port.1.RMONStats.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(uint32_t, &trans, "Bytes", value);
    amxd_trans_set_value(uint32_t, &trans, "Packets", value);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    // Create an RMONStats instance
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.RMONStats.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Interface", ETH_INTF_TEST);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    // Now get Ethernet stats, which will in turn get Port stats
    amxb_get(amxut_bus_ctx(), "Ethernet.RMONStats.1.", 0, &ret, 5);
    assert_int_equal(GETP_UINT32(&ret, "0.0.Bytes"), value);
    assert_int_equal(GETP_UINT32(&ret, "0.0.Packets"), value);
    assert_int_equal(GETP_UINT32(&ret, "0.0.DropEvents"), 0);

    amxc_var_clean(&ret);
}

void test_can_sync_mac_address(UNUSED void** state) {
    amxc_var_t ret;
    amxd_trans_t trans;
    const char* new_mac = "11:22:33:44:55:66";

    amxc_var_init(&ret);

    // Initial MACAddress is already synced from CPU_SWITCHPORT Interface
    amxb_get(amxut_bus_ctx(), ETH_INTF_TEST "MACAddress", 0, &ret, 5);
    assert_string_equal(GETP_CHAR(&ret, "0.0.MACAddress"), "AA:BB:CC:DD:EE:FF");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.CPU_SWITCHPORT.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", new_mac);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    // MACAddress is kept in sync
    amxb_get(amxut_bus_ctx(), ETH_INTF_TEST "MACAddress", 0, &ret, 5);
    assert_string_equal(GETP_CHAR(&ret, "0.0.MACAddress"), new_mac);

    amxc_var_clean(&ret);
}
