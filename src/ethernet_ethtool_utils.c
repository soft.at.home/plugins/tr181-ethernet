/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_utils.h"
#include "ethernet_soc_utils.h"
#include "ethernet_ethtool_utils.h"

#define ME "eth-utils"

int dm_ethernet_ethtool_update(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_dm_t* dm = ethernet_get_dm();
    amxd_trans_t trans;
    const char* ctrl = NULL;
    amxc_var_t data;
    amxc_var_t ret;

    amxd_trans_init(&trans);
    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_null_trace(ethernet, exit, ERROR, "object can not be NULL, no valid path");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");
    when_null_trace(ethernet->netdev_intf, exit, ERROR, "Netdev interface name must be set");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    // Add mandatory arguments
    amxc_var_add_key(cstring_t, &data, "intf_name", ethernet->netdev_intf);

    ctrl = mod_ctrl_get(ethernet->object, ETH_LLA_CTRL);
    SAH_TRACEZ_INFO(ME, "Calling link-settings-get implementation (controller = '%s')", ctrl);
    rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "link-settings-get", &data, &ret);
    when_failed_trace(rv, exit, INFO, "Failed to call link-settings-get, return '%d'", rv)

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ethernet->object);
    amxd_trans_set_value(int32_t, &trans, "CurrentBitRate", GET_INT32(&ret, "current_bit_rate"));
    amxd_trans_set_value(cstring_t, &trans, "CurrentDuplexMode", GET_CHAR(&ret, "current_duplex_mode"));
    rv = amxd_trans_apply(&trans, dm);

exit:
    amxd_trans_clean(&trans);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int dm_ethernet_ethtool_set(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* ctrl = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_null_trace(ethernet, exit, ERROR, "object can not be NULL, no valid path");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");
    when_null_trace(ethernet->netdev_intf, exit, ERROR, "Netdev interface name must be set");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &data, "ethernet", NULL);
    amxd_object_get_params(ethernet->object, params, amxd_dm_access_protected);

    // Add mandatory arguments
    amxc_var_add_key(cstring_t, &data, "intf_name", ethernet->netdev_intf);
    amxc_var_add_key(cstring_t, &data, "duplex_mode", GET_CHAR(params, "DuplexMode"));
    amxc_var_add_key(int32_t, &data, "max_bit_rate", GET_INT32(params, "MaxBitRate"));

    ctrl = mod_ctrl_get(ethernet->object, ETH_LLA_CTRL);
    SAH_TRACEZ_INFO(ME, "Calling link-settings-set implementation (controller = '%s')", ctrl);
    rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "link-settings-set", &data, &ret);
    when_failed_trace(rv, exit, INFO, "Failed to call link-settings-set, return '%d'", rv);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
