/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_utils.h"
#include "ethernet_soc_utils.h"
#include "ethernet_ethtool_utils.h"
#include "ethernet_mac.h"

#define ME "utils"

/**
 * @brief callback function for when the NetDev status changes
 */
static void _netdev_link_state_cb(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    SAH_TRACEZ_IN(ME);
    ethernet_info_t* ethernet = NULL;
    const char* new_state = NULL;
    const char* old_state = NULL;

    when_null_trace(priv, exit, ERROR, "private data should contain data");
    ethernet = (ethernet_info_t*) priv;

    old_state = GETP_CHAR(data, "parameters.State.from");
    new_state = GETP_CHAR(data, "parameters.State.to");

    SAH_TRACEZ_INFO(ME, "%s State changed from %s to %s", ethernet->object->name, old_state, new_state);
    dm_ethernet_update_status(ethernet, new_state);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief callback function for when the NetDev LLAddress changes
 */
static void _mac_address_update_cb(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   void* const priv) {
    SAH_TRACEZ_IN(ME);
    ethernet_info_t* ethernet = NULL;
    const char* new_mac = NULL;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    when_null_trace(priv, exit, ERROR, "private data should contain data");
    ethernet = (ethernet_info_t*) priv;

    new_mac = GETP_CHAR(data, "parameters.LLAddress.to");
    when_str_empty(new_mac, exit);

    SAH_TRACEZ_INFO(ME, "LLAddress for %s changed to %s", ethernet->object->name, new_mac);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ethernet->object);
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", new_mac);
    when_failed_trace(amxd_trans_apply(&trans, ethernet_get_dm()), exit,
                      ERROR, "failed to update MACAddress to %s", new_mac);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Sets the Status parameter for the ethernet->object to the current State value in NetDev
 * @param ethernet pointer to ethernet info structure
 * @param ctx pointer to the bus context that provides NetDev.Link.
 */
static void netdev_link_get_initial_state(ethernet_info_t* ethernet, amxb_bus_ctx_t* ctx) {
    SAH_TRACEZ_IN(ME);
    int rv = AMXB_ERROR_UNKNOWN;
    amxc_var_t data;
    const char* new_state = NULL;
    amxc_string_t rel_path;

    amxc_string_init(&rel_path, 0);
    amxc_var_init(&data);

    when_null(ethernet, exit);

    if(ctx != NULL) {
        amxc_string_setf(&rel_path, "NetDev.Link.[Name==\"%s\"].State", ethernet->netdev_intf);
        rv = amxb_get(ctx, amxc_string_get(&rel_path, 0), 0, &data, 1);
        if(rv != AMXB_STATUS_OK) {
            SAH_TRACEZ_ERROR(ME, "NetDev link[%s] not found", ethernet->netdev_intf);
        } else {
            new_state = GETP_CHAR(&data, "0.0.State");
        }
    }
    if(new_state == NULL) {
        SAH_TRACEZ_ERROR(ME, "netdev status for %s could not be found", ethernet->netdev_intf);
        new_state = STATUS_ERROR;
    }

    dm_ethernet_update_status(ethernet, new_state);

exit:
    amxc_string_clean(&rel_path);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

static void netdev_link_get_initial_mac(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = AMXB_ERROR_UNKNOWN;
    amxb_bus_ctx_t* ctx = ethernet_get_netdev_ctx();
    amxc_string_t rel_path;
    amxc_var_t data;
    amxd_trans_t trans;
    const char* mac_addr = NULL;

    amxc_string_init(&rel_path, 0);
    amxc_var_init(&data);
    amxd_trans_init(&trans);

    when_null_trace(ctx, exit, ERROR, "failed to find a bus context providing NetDev")

    amxc_string_setf(&rel_path, "NetDev.Link.[Name==\"%s\"].LLAddress", ethernet->netdev_intf);
    rv = amxb_get(ctx, amxc_string_get(&rel_path, 0), 0, &data, 1);
    when_failed_trace(rv, exit, ERROR, "NetDev link[%s] not found", ethernet->netdev_intf)

    mac_addr = GETP_CHAR(&data, "0.0.LLAddress");
    when_str_empty(mac_addr, exit);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ethernet->object);
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", mac_addr);
    when_failed_trace(amxd_trans_apply(&trans, ethernet_get_dm()), exit,
                      ERROR, "failed to update MACAddress to %s", mac_addr);

exit:
    amxc_string_clean(&rel_path);
    amxc_var_clean(&data);
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief callback function for when a new Link is added to NetDev or
 * the name of an existing link changes
 */
static void _netdev_new_link_added_cb(UNUSED const char* const sig_name,
                                      UNUSED const amxc_var_t* const data,
                                      void* const priv) {
    SAH_TRACEZ_IN(ME);
    ethernet_info_t* ethernet = NULL;

    when_null_trace(priv, exit, ERROR, "private data should data");
    ethernet = (ethernet_info_t*) priv;

    SAH_TRACEZ_INFO(ME, "%s interface was added to NetDev", ethernet->netdev_intf);
    netdev_link_get_initial_state(ethernet, ethernet_get_netdev_ctx());

    if(ethernet->type == ETHERNET_INTERFACE) {
        netdev_link_get_initial_mac(ethernet);
    } else if(ethernet->type == ETHERNET_LINK) {
        handle_mac_address(ethernet);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t reset_ethtool(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_dm_t* dm = ethernet_get_dm();
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    when_null_trace(ethernet, exit, ERROR, "Ethernet info can not be NULL, can not change state");
    when_null_trace(ethernet->object, exit, ERROR, "object should not be NULL, can not change state");

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ethernet->object);
    amxd_trans_set_value(int32_t, &trans, "CurrentBitRate", 0);
    amxd_trans_set_value(cstring_t, &trans, "CurrentDuplexMode", "Unknown");
    rv = amxd_trans_apply(&trans, dm);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Changes the objects status in the datamodel
 * @param object the object pointer
 * @param status char* value containing the NetDev state
 * @return amxd_status_ok when the all actions are applied, otherwise an other error code and no changes in the data model are done.
 */
amxd_status_t dm_ethernet_update_status(ethernet_info_t* ethernet, const char* status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = ethernet_get_dm();
    amxd_trans_t trans;
    const char* new_status = NULL;
    amxd_trans_init(&trans);

    when_null_trace(ethernet, exit, ERROR, "Ethernet info can not be NULL, can not change state");
    when_null_trace(ethernet->object, exit, ERROR, "object should not be NULL, can not change state");
    when_str_empty(status, exit);

    if(strcmp(status, "notpresent") == 0) {
        new_status = "NotPresent";
    } else if(strcmp(status, "down") == 0) {
        new_status = "Down";
    } else if(strcmp(status, "lowerlayerdown") == 0) {
        new_status = "LowerLayerDown";
    } else if(strcmp(status, "dormant") == 0) {
        new_status = "Dormant";
    } else if(strcmp(status, "up") == 0) {
        new_status = "Up";
    } else if(strcmp(status, "Error") == 0) {
        new_status = "Error";
    } else {
        new_status = "Unknown";
    }

    if(strcmp(status, "up") == 0) {
        if(ethernet->type == ETHERNET_INTERFACE) {
            dm_ethernet_ethtool_update(ethernet);
        }
        handle_mac_address(ethernet);
    } else {
        reset_ethtool(ethernet);
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ethernet->object);
    amxd_trans_set_value(cstring_t, &trans, "Status", new_status);
    rv = amxd_trans_apply(&trans, dm);
    ethernet->last_change = get_system_uptime();

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Create a new subscription on NetDev to listen to add events for this ethernet instance
 * @param ethernet pointer to the ethernet info structure where to store the subscription
 */
void netdev_link_added_subscription_new(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;

    amxc_string_init(&expr, 0);

    when_null(ethernet, exit);
    when_null_trace(ethernet->netdev_intf, exit, ERROR, "not a valid netdev interface");

    ctx = ethernet_get_netdev_ctx();
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "notification == 'dm:instance-added' && path == 'NetDev.Link.' " \
                     "&& parameters.Alias == '%s'", ethernet->netdev_intf);

    amxb_subscription_new(&(ethernet->netdev_new_link_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_new_link_added_cb, ethernet);
    when_null_trace(ethernet->netdev_new_link_subscription, exit, WARNING, "failed to create subscription");

exit:
    amxc_string_clean(&expr);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Create a new subscription on NetDev that listens for changes to State
 * @param ethernet pointer to the ethernet info structure where to store the subscription
 */
void netdev_link_state_subscription_new(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;

    amxc_string_init(&expr, 0);

    when_null(ethernet, exit);
    when_str_empty_trace(ethernet->netdev_intf, exit, ERROR, "not a valid netdev interface");

    ctx = ethernet_get_netdev_ctx();
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "eobject == 'NetDev.Link.[%s].' && "
                     "contains('parameters.State')", ethernet->netdev_intf);
    if(ethernet->netdev_status_subscription != NULL) {
        amxb_subscription_delete(&(ethernet->netdev_status_subscription));
    }

    amxb_subscription_new(&(ethernet->netdev_status_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_link_state_cb, ethernet);
    when_null_trace(ethernet->netdev_status_subscription, exit, WARNING, "failed to create subscription");
    netdev_link_get_initial_state(ethernet, ctx);

exit:
    amxc_string_clean(&expr);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Create a new subscription on NetDev that listens for changes to LLAddress
 * @param ethernet pointer to the ethernet info structure where to store the subscription
 */
void netdev_link_mac_address_subscription_new(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;

    amxc_string_init(&expr, 0);

    when_null(ethernet, exit);
    when_str_empty_trace(ethernet->netdev_intf, exit, ERROR, "not a valid netdev interface");

    ctx = ethernet_get_netdev_ctx();
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "eobject == 'NetDev.Link.[%s].' && "
                     "contains('parameters.LLAddress')", ethernet->netdev_intf);
    if(ethernet->netdev_mac_subscription != NULL) {
        amxb_subscription_delete(&(ethernet->netdev_mac_subscription));
    }

    amxb_subscription_new(&(ethernet->netdev_mac_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _mac_address_update_cb, ethernet);
    when_null_trace(ethernet->netdev_mac_subscription, exit, WARNING, "failed to create subscription");
    netdev_link_get_initial_mac(ethernet);

exit:
    amxc_string_clean(&expr);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Initialize a new ethernet info structure
 * @param ethernet pointer to ethernet info structure
 */
amxd_status_t ethernet_info_new(ethernet_info_t** ethernet, const char* netdev_intf) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(ethernet, exit, ERROR, "interface can not be null, invalid argument");
    when_str_empty(netdev_intf, exit);

    *ethernet = (ethernet_info_t*) calloc(1, sizeof(ethernet_info_t));
    when_null_status(*ethernet, exit, rv = amxd_status_out_of_mem);

    (*ethernet)->last_change = get_system_uptime();
    (*ethernet)->netdev_intf = strndup(netdev_intf, 64);
    when_null_status((*ethernet)->netdev_intf, clean, rv = amxd_status_unknown_error);

    rv = amxd_status_ok;

clean:
    if(rv != amxd_status_ok) {
        free(*ethernet);
        *ethernet = NULL;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Cleanup a ethernet info structure
 * @param ethernet pointer to the ethernet info structure
 */
amxd_status_t ethernet_info_clean(ethernet_info_t** ethernet) {
    SAH_TRACEZ_IN(ME);
    if(!ethernet || !(*ethernet)) {
        goto exit;
    }

    amxs_sync_ctx_delete(&(*ethernet)->sync_to_switch);
    amxs_sync_ctx_delete(&(*ethernet)->sync_from_switch);
    amxs_sync_ctx_delete(&(*ethernet)->sync_from_cpu_port);
    free((*ethernet)->netdev_intf);
    amxb_subscription_delete(&(*ethernet)->netdev_status_subscription);
    amxb_subscription_delete(&(*ethernet)->netdev_new_link_subscription);
    amxb_subscription_delete(&(*ethernet)->netdev_mac_subscription);
    free((*ethernet)->link_name);
    free(*ethernet);
    *ethernet = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

/**
 * @brief Enable or disable an interface or link.
 *  Links can only be toggled if the LowerLayers parameter is empty.
 * @param ethernet pointer to the ethernet info structure
 * @param enable Boolean to indicate whether the interface should be
 *               enabled or disabled
 * @return 0 in case of success, non 0 otherwise
 */
int ethernet_info_set_enabled(ethernet_info_t* ethernet, bool enable) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;

    when_null_trace(ethernet, exit, ERROR, "interface can not be null, invalid argument");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");
    if(ethernet->type != ETHERNET_INTERFACE) {
        char* llayers = amxd_object_get_value(cstring_t, ethernet->object, "LowerLayers", NULL);
        if((llayers != NULL) && (*llayers != 0)) {
            SAH_TRACEZ_INFO(ME, "Link Enable only works for Links with no LowerLayers set");
            ret = 0;
            free(llayers);
            goto exit;
        }
        free(llayers);
    }

    ret = set_ethernet_enabled(ethernet->netdev_intf, ethernet->object, enable);

exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

int link_disable_arp(ethernet_info_t* ethernet, bool noarp) {
    int rv = -1;
    amxc_var_t data;
    amxc_var_t ret;
    const char* ctrl = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_null_trace(ethernet, exit, ERROR, "interface can not be null, invalid argument");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");
    ctrl = mod_ctrl_get(ethernet->object, ETH_LLA_CTRL);

    when_str_empty_trace(ethernet->netdev_intf, exit, ERROR, "Interface name can not be empty");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "intf_name", ethernet->netdev_intf);
    amxc_var_add_key(bool, &data, "noarp", noarp);

    SAH_TRACEZ_INFO(ME, "Calling link-disable-arp implementation (controller = '%s')", ctrl);
    rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "link-disable-arp", &data, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to call link-disable-arp, return '%d'", rv);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return rv;
}

uint32_t get_system_uptime(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    uint32_t uptime = 0;
    amxc_var_t data;
    amxc_var_t ret;
    const char* ctrl = mod_ctrl_get(NULL, ETH_LLA_CTRL);

    amxc_var_init(&data);
    amxc_var_init(&ret);

    SAH_TRACEZ_INFO(ME, "Calling system-uptime-get implementation (controller = '%s')", ctrl);
    rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "system-uptime-get", &data, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to call system-uptime-get, return '%d'", rv)
    uptime = GET_UINT32(&ret, "uptime");

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return uptime;
}

/**
 * @brief Initialize a new ethernet info structure
 * @param ethernet pointer to ethernet info structure
 */
amxd_status_t rmonstats_info_new(ethernet_rmonstats_info_t** rmonstats_info, bool enable) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(rmonstats_info, exit, ERROR, "interface can not be null, invalid argument");

    *rmonstats_info = (ethernet_rmonstats_info_t*) calloc(1, sizeof(ethernet_rmonstats_info_t));
    when_null_status(*rmonstats_info, exit, rv = amxd_status_out_of_mem);

    (*rmonstats_info)->enable = enable;
    (*rmonstats_info)->status = enable ? RMONSTATS_ENABLED : RMONSTATS_DISABLED;
    (*rmonstats_info)->object = NULL;

    rv = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Cleanup a ethernet info structure
 * @param ethernet pointer to the ethernet info structure
 */
amxd_status_t rmonstats_info_clean(ethernet_rmonstats_info_t** rmonstats_info) {
    SAH_TRACEZ_IN(ME);
    if(!rmonstats_info || !(*rmonstats_info)) {
        goto exit;
    }

    free((*rmonstats_info)->interface);
    free(*rmonstats_info);
    *rmonstats_info = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}
