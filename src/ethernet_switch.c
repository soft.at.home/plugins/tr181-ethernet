/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_switch.h"

#define ME "ethernet-switch"

/**
 *  Struct used to create a list of information about the stats parameters
 */
typedef struct StatsParameter {
    const char* name;     // Name of the parameter
    uint32_t type;        // Parameter type
} StatsParameter_t;

/**
 * Definition for the stats parameters
 */
static StatsParameter_t StatsParameters[] = {
    {"BytesSent", AMXC_VAR_ID_UINT64},
    {"BytesReceived", AMXC_VAR_ID_UINT64},
    {"PacketsSent", AMXC_VAR_ID_UINT64},
    {"PacketsReceived", AMXC_VAR_ID_UINT64},
    {"ErrorsSent", AMXC_VAR_ID_UINT32},
    {"ErrorsReceived", AMXC_VAR_ID_UINT32},
    {"UnicastPacketsSent", AMXC_VAR_ID_UINT64},
    {"UnicastPacketsReceived", AMXC_VAR_ID_UINT64},
    {"DiscardPacketsSent", AMXC_VAR_ID_UINT32},
    {"DiscardPacketsReceived", AMXC_VAR_ID_UINT32},
    {"MulticastPacketsSent", AMXC_VAR_ID_UINT64},
    {"MulticastPacketsReceived", AMXC_VAR_ID_UINT64},
    {"BroadcastPacketsSent", AMXC_VAR_ID_UINT64},
    {"BroadcastPacketsReceived", AMXC_VAR_ID_UINT64},
    {"UnknownProtoPacketsReceived", AMXC_VAR_ID_UINT32},
    { NULL, 0}
};

static StatsParameter_t RMONStatsParameters[] = {
    {"Bytes", AMXC_VAR_ID_UINT64},
    {"Packets", AMXC_VAR_ID_UINT64},
    {"DropEvents", AMXC_VAR_ID_UINT32},
    {"BroadcastPackets", AMXC_VAR_ID_UINT64},
    {"MulticastPackets", AMXC_VAR_ID_UINT64},
    {"CRCErroredPackets", AMXC_VAR_ID_UINT32},
    {"UndersizePackets", AMXC_VAR_ID_UINT32},
    {"OversizePackets", AMXC_VAR_ID_UINT32},
    {"Packets64Bytes", AMXC_VAR_ID_UINT64},
    {"Packets65to127Bytes", AMXC_VAR_ID_UINT64},
    {"Packets128to255Bytes", AMXC_VAR_ID_UINT64},
    {"Packets256to511Bytes", AMXC_VAR_ID_UINT64},
    {"Packets512to1023Bytes", AMXC_VAR_ID_UINT64},
    {"Packets1024to1518Bytes", AMXC_VAR_ID_UINT64},
    { NULL, 0}
};

static amxd_status_t ethernet_switch_populate_stats(amxc_var_t* const retval,
                                                    const StatsParameter_t* params,
                                                    amxc_var_t* stats,
                                                    const char* requested_param) {
    for(int i = 0; params[i].name != NULL; i++) {
        amxc_var_t* stat_val = NULL;

        // Make sure Stats are not returned when they are not requested
        if((requested_param != NULL) && (strcmp(requested_param, params[i].name) != 0)) {
            continue;
        }

        stat_val = amxc_var_get_key(stats, params[i].name, AMXC_VAR_FLAG_DEFAULT);
        if(stat_val == NULL) {
            amxc_var_add_key(uint64_t, retval, params[i].name, 0);
        } else {
            amxc_var_take_it(stat_val);
            amxc_var_set_key(retval, params[i].name, stat_val, AMXC_VAR_FLAG_UPDATE);
        }
        amxc_var_cast(stat_val, params[i].type);
    }

    return amxd_status_ok;
}

static amxd_status_t ethernet_switch_get_stats(amxd_object_t* const object,
                                               UNUSED amxd_param_t* const param,
                                               amxd_action_t reason,
                                               const amxc_var_t* const args,
                                               amxc_var_t* const retval,
                                               UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("Switch.");
    amxd_object_t* interface_obj = amxd_object_get_parent(object);
    const char* switch_port = GET_CHAR(amxd_object_get_param_value(interface_obj, "SwitchPort"), NULL);
    const char* requested_param = GETP_CHAR(args, "parameters.0");
    amxc_string_t stats_obj;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&stats_obj, 0);

    if(reason != action_object_read) {
        SAH_TRACEZ_WARNING(ME, "wrong reason, expected action_object_read(%d) got %d", action_object_read, reason);
        status = amxd_status_invalid_action;
        goto exit;
    }

    when_str_empty(switch_port, exit);
    amxc_string_setf(&stats_obj, "%s.Stats.", switch_port);

    // Get different object depending on interface
    status = amxb_get(ctx, amxc_string_get(&stats_obj, 0), 0, &ret, 5);
    when_failed_trace(status, exit, ERROR, "Failed to get %s", amxc_string_get(&stats_obj, 0));

    amxc_var_set_type(retval, AMXC_VAR_ID_HTABLE);
    status = ethernet_switch_populate_stats(retval, StatsParameters, GETP_ARG(&ret, "0.0"), requested_param);
    when_failed_trace(status, exit, ERROR, "Failed to set Stats parameters");

exit:
    amxc_string_clean(&stats_obj);
    amxc_var_clean(&ret);
    return status;
}

static amxd_status_t ethernet_switch_get_rmon_stats(amxd_object_t* const object,
                                                    UNUSED amxd_param_t* const param,
                                                    amxd_action_t reason,
                                                    const amxc_var_t* const args,
                                                    amxc_var_t* const retval,
                                                    UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("Switch.");
    amxd_object_t* interface_obj = NULL;
    ethernet_rmonstats_info_t* rmonstats_info = (ethernet_rmonstats_info_t*) object->priv;
    const char* switch_port = NULL;
    const char* requested_param = GETP_CHAR(args, "parameters.0");
    amxc_string_t rmonstats_obj;
    amxc_var_t ret;

    status = amxd_action_object_read(object, param, reason, args, retval, priv);
    when_failed_trace(status, exit, ERROR, "Failed to call default read action");

    amxc_var_init(&ret);
    amxc_string_init(&rmonstats_obj, 0);

    when_null(rmonstats_info, exit);
    when_str_empty(rmonstats_info->interface, exit);

    interface_obj = amxd_dm_findf(ethernet_get_dm(), "%s", rmonstats_info->interface);
    switch_port = GET_CHAR(amxd_object_get_param_value(interface_obj, "SwitchPort"), NULL);

    if(reason != action_object_read) {
        SAH_TRACEZ_WARNING(ME, "wrong reason, expected action_object_read(%d) got %d", action_object_read, reason);
        status = amxd_status_invalid_action;
        goto exit;
    }

    when_str_empty(switch_port, exit);
    amxc_string_setf(&rmonstats_obj, "%s.RMONStats.", switch_port);

    // Get different object depending on interface
    status = amxb_get(ctx, amxc_string_get(&rmonstats_obj, 0), 0, &ret, 5);
    when_failed_trace(status, exit, ERROR, "Failed to get %s", amxc_string_get(&rmonstats_obj, 0));

    status = ethernet_switch_populate_stats(retval, RMONStatsParameters, GETP_ARG(&ret, "0.0"), requested_param);
    when_failed_trace(status, exit, ERROR, "Failed to set Stats parameters");

exit:
    amxc_string_clean(&rmonstats_obj);
    amxc_var_clean(&ret);
    return status;
}

static amxs_status_t ethernet_switch_sync_led_to_switch(amxs_sync_ctx_t* ctx) {
    amxs_status_t status = amxs_status_ok;
    amxs_sync_object_t* led_obj = NULL;
    amxo_parser_t* parser = ethernet_get_parser();
    const char* prefix = GET_CHAR(amxo_parser_get_config(parser, "prefix_"), NULL);
    amxc_string_t led_obj_name;

    amxc_string_init(&led_obj_name, 0);
    amxc_string_setf(&led_obj_name, "%sLED.", prefix == NULL ? "" : prefix);

    status = amxs_sync_object_new_copy(&led_obj, "LED.", amxc_string_get(&led_obj_name, 0), AMXS_SYNC_ONLY_B_TO_A | AMXS_SYNC_INIT_B);
    status |= amxs_sync_object_add_new_copy_param(led_obj, "Enable", "Enable", AMXS_SYNC_ONLY_B_TO_A | AMXS_SYNC_INIT_B);
    status |= amxs_sync_ctx_add_object(ctx, led_obj);

    amxc_string_clean(&led_obj_name);
    return status;
}

static amxs_status_t ethernet_switch_sync_to_switch(amxs_sync_ctx_t** ctx,
                                                    const char* switch_path,
                                                    const char* ethernet_path) {
    amxs_status_t status = amxs_status_ok;

    status = amxs_sync_ctx_new(ctx, switch_path,
                               ethernet_path, AMXS_SYNC_ONLY_B_TO_A | AMXS_SYNC_INIT_B);
    when_failed_trace(status, exit, ERROR, "Failed to create sync ctx, status: %d", status);

    // Sync parameters from Ethernet.Interface to Switch.Port
    status |= amxs_sync_ctx_add_new_copy_param(*ctx, "Enable", "Enable",
                                               AMXS_SYNC_ONLY_B_TO_A | AMXS_SYNC_INIT_B);
    status |= amxs_sync_ctx_add_new_copy_param(*ctx, "DuplexMode", "DuplexMode",
                                               AMXS_SYNC_ONLY_B_TO_A | AMXS_SYNC_INIT_B);
    status |= amxs_sync_ctx_add_new_copy_param(*ctx, "EEEEnable", "EEEEnable",
                                               AMXS_SYNC_ONLY_B_TO_A | AMXS_SYNC_INIT_B);
    status |= amxs_sync_ctx_add_new_copy_param(*ctx, "MaxBitRate", "MaxBitRate",
                                               AMXS_SYNC_ONLY_B_TO_A | AMXS_SYNC_INIT_B);
    when_failed_trace(status, exit, ERROR, "Failed to sync params from Ethernet to Switch, status: %d", status);

    // Sync Ethernet.Interface.x.LED.Enable to Switch.Port.x.LED.Enable
    status = ethernet_switch_sync_led_to_switch(*ctx);
    when_failed_trace(status, exit, ERROR, "Failed to sync LED.Enable from Ethernet to Switch, status: %d", status);

    status = amxs_sync_ctx_start_sync(*ctx);
    when_failed_trace(status, exit, ERROR, "Failed to start sync, status: %d", status);

exit:
    return status;
}

static amxs_status_t ethernet_switch_sync_from_switch(amxs_sync_ctx_t** ctx,
                                                      const char* switch_path,
                                                      const char* ethernet_path) {
    amxs_status_t status = amxs_status_ok;

    status = amxs_sync_ctx_new(ctx, switch_path, ethernet_path, AMXS_SYNC_ONLY_A_TO_B);
    when_failed_trace(status, exit, ERROR, "Failed to create sync ctx, status: %d", status);

    status = amxs_sync_ctx_set_local_dm(*ctx, NULL, ethernet_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to set local dm, status: %d", status);

    // Sync parameters from Switch.Port to Ethernet.Interface
    status = amxs_sync_ctx_add_new_copy_param(*ctx, "PortStatus", "Status",
                                              AMXS_SYNC_ONLY_A_TO_B);
    status |= amxs_sync_ctx_add_new_copy_param(*ctx, "CurrentBitRate", "CurrentBitRate",
                                               AMXS_SYNC_ONLY_A_TO_B);
    status |= amxs_sync_ctx_add_new_copy_param(*ctx, "EEECapability", "EEECapability",
                                               AMXS_SYNC_ONLY_A_TO_B);
    when_failed_trace(status, exit, ERROR, "Failed to sync params from Switch to Ethernet, status: %d", status);

    status = amxs_sync_ctx_start_sync(*ctx);
    when_failed_trace(status, exit, ERROR, "Failed to start sync, status: %d", status);

exit:
    return status;
}

static amxs_status_t ethernet_switch_sync_from_cpu_port(amxs_sync_ctx_t** ctx,
                                                        const char* ethernet_path) {
    amxs_status_t status = amxs_status_unknown_error;
    amxc_var_t* config = amxo_parser_get_config(ethernet_get_parser(), "cpu-port");
    const char* cpu_port_path = GET_CHAR(config, NULL);

    when_str_empty_trace(cpu_port_path, exit, WARNING, "No cpu-port config option found");

    status = amxs_sync_ctx_new(ctx, cpu_port_path, ethernet_path, AMXS_SYNC_ONLY_A_TO_B);
    when_failed_trace(status, exit, ERROR, "Failed to create sync ctx, status: %d", status);

    status = amxs_sync_ctx_set_local_dm(*ctx, NULL, ethernet_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to set local dm, status: %d", status);

    // Sync MAC address from CPU port to Ethernet Interface that corresponds to a switch port
    status = amxs_sync_ctx_add_new_copy_param(*ctx, "MACAddress", "MACAddress",
                                              AMXS_SYNC_ONLY_A_TO_B);
    when_failed_trace(status, exit, ERROR, "Failed to sync params from CPU port interface to Ethernet interface, status: %d", status);

    status = amxs_sync_ctx_start_sync(*ctx);
    when_failed_trace(status, exit, ERROR, "Failed to start sync, status: %d", status);

exit:
    return status;
}

static void ethernet_switch_setup_sync(amxd_object_t* interface_obj,
                                       ethernet_info_t* ethernet,
                                       const char* switch_port) {
    amxs_status_t status = amxs_status_ok;
    amxc_string_t instance_path;
    amxc_string_t switch_path;

    amxc_string_init(&instance_path, 0);
    amxc_string_setf(&instance_path, "Ethernet.Interface.%d.", amxd_object_get_index(interface_obj));
    amxc_string_init(&switch_path, 0);
    amxc_string_setf(&switch_path, "%s.", switch_port);

    when_null(ethernet, exit);

    status = ethernet_switch_sync_to_switch(&ethernet->sync_to_switch, amxc_string_get(&switch_path, 0),
                                            amxc_string_get(&instance_path, 0));
    when_failed_trace(status, exit, ERROR, "Failed to setup sync to switch, status: %d", status);

    status = ethernet_switch_sync_from_switch(&ethernet->sync_from_switch, amxc_string_get(&switch_path, 0),
                                              amxc_string_get(&instance_path, 0));
    when_failed_trace(status, exit, ERROR, "Failed to setup sync from switch, status: %d", status);

    status = ethernet_switch_sync_from_cpu_port(&ethernet->sync_from_cpu_port,
                                                amxc_string_get(&instance_path, 0));
    when_failed_trace(status, exit, ERROR, "Failed to setup sync from cpu port, status: %d", status);

exit:
    amxc_string_clean(&switch_path);
    amxc_string_clean(&instance_path);
}

void ethernet_switch_init(amxd_object_t* interface_obj,
                          ethernet_info_t* ethernet,
                          const char* switch_port) {
    amxd_object_t* stats = amxd_object_findf(interface_obj, "Stats.");

    ethernet_switch_setup_sync(interface_obj, ethernet, switch_port);
    amxd_object_add_action_cb(stats, action_object_read, ethernet_switch_get_stats, NULL);
}

void ethernet_switch_rmon_stats_init(amxd_object_t* rmonstats_obj, const char* intf) {
    amxd_object_t* interface_obj = amxd_dm_findf(ethernet_get_dm(), "%s", intf);
    const char* switch_port = NULL;

    when_null_trace(interface_obj, exit, ERROR, "Failed to get Interface object");

    // Exit if no link with switch port
    switch_port = GET_CHAR(amxd_object_get_param_value(interface_obj, "SwitchPort"), NULL);
    when_str_empty(switch_port, exit);

    amxd_object_add_action_cb(rmonstats_obj, action_object_read, ethernet_switch_get_rmon_stats, NULL);

exit:
    return;
}