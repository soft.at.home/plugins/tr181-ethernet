/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_netdev_stats.h"

#define ME "ethernet"

static int netdev_rmonstats_ret_create(amxc_var_t* netdev_link_child, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxc_var_t* help_var = NULL;

    when_null_trace(netdev_link_child, exit, ERROR, "netdev_link_child is null");
    when_null_trace(ret, exit, ERROR, "Ret variant is null");

    help_var = amxc_var_get_key(ret, "Bytes", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, help_var, GET_UINT32(netdev_link_child, "RxBytes"));
    help_var = amxc_var_get_key(ret, "CRCErroredPackets", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, help_var, GET_UINT32(netdev_link_child, "RxCrcErrors"));
    help_var = amxc_var_get_key(ret, "DropEvents", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, help_var, GET_UINT32(netdev_link_child, "RxDropped"));
    help_var = amxc_var_get_key(ret, "MulticastPackets", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, help_var, GET_UINT32(netdev_link_child, "Multicast"));
    help_var = amxc_var_get_key(ret, "OversizePackets", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, help_var, GET_UINT32(netdev_link_child, "RxOverErrors"));
    help_var = amxc_var_get_key(ret, "Packets", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, help_var, GET_UINT32(netdev_link_child, "RxPackets"));
    help_var = amxc_var_get_key(ret, "UndersizePackets", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, help_var, GET_UINT32(netdev_link_child, "RxLengthErrors"));

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int get_stats_rmonstats(UNUSED const char* function_name,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);

    amxd_status_t rv = amxd_status_unknown_error;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev.");
    amxc_var_t netdev_link;
    amxc_var_t* netdev_link_child = NULL;
    amxc_string_t netdev_path;
    const char* netdev_intf = GET_CHAR(args, "network_intf");

    amxc_var_init(&netdev_link);
    amxc_string_init(&netdev_path, 0);

    when_str_empty_trace(netdev_intf, exit, ERROR, "No interface name specified");

    amxc_string_setf(&netdev_path, "NetDev.Link.[Alias == '%s'].Stats.", netdev_intf);

    rv = amxb_get(ctx, amxc_string_get(&netdev_path, 0), 0, &netdev_link, 5);
    when_failed_trace(rv, exit, ERROR, "Could not get stats variant for RMONStats");

    netdev_link_child = GETP_ARG(&netdev_link, "0.0");
    when_null_status(netdev_link_child, exit, rv = amxd_status_unknown_error);

    rv = netdev_rmonstats_ret_create(netdev_link_child, ret);

exit:
    amxc_var_clean(&netdev_link);
    amxc_string_clean(&netdev_path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static AMXM_CONSTRUCTOR module_start(void) {
    SAH_TRACEZ_IN(ME);
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_RMONSTATS_CTRL);
    amxm_module_add_function(mod, "get-stats-rmonstats", get_stats_rmonstats);

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_DESTRUCTOR module_stop(void) {

    return 0;
}
