/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <regex.h>
#include <linux/sockios.h>

#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_util.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../../include_priv/mod_netdev_stats.h"
#include "test_module.h"

#define MOD_NAME "target_module"

int test_setup(UNUSED void** state) {

    return 0;
}

int test_teardown(UNUSED void** state) {

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, MOD_NAME, "../modules_under_test/target_module.so"), 0);
}

void test_has_correct_functions(UNUSED void** state) {
    assert_true(amxm_has_function(MOD_NAME, MOD_RMONSTATS_CTRL, "get-stats-rmonstats"));
}

void test_can_fetch_netdev_data(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t* on_read = NULL;

    amxc_var_init(&var);

    on_read = amxut_util_read_json_from_file("test_data/on_read_rmonstats_data.json");

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "link_name", "eth0");
    amxc_var_add_key(cstring_t, &var, "network_intf", "eth0");
    amxc_var_add_key(uint32_t, &var, "VLANID", 10);
    amxc_var_add_key(bool, &var, "AllQueues", true);
    amxc_var_add_key(cstring_t, &var, "Queue", "");

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_RMONSTATS_CTRL, "get-stats-rmonstats", &var, on_read), 0);

    assert_int_equal(GET_UINT32(on_read, "CRCErroredPackets"), 1000);
    assert_int_equal(GET_UINT32(on_read, "Bytes"), 1000);
    assert_int_equal(GET_UINT32(on_read, "Packets"), 1000);

    amxc_var_clean(&var);
    amxc_var_delete(&on_read);
}

void test_can_close_module(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
}

int __wrap_amxb_get(UNUSED amxb_bus_ctx_t* const bus_ctx, UNUSED const char* object, UNUSED int32_t depth, UNUSED amxc_var_t* ret, UNUSED int timeout) {
    amxc_var_t* data = NULL;
    data = amxut_util_read_json_from_file("test_data/netdev_data.json");
    amxc_var_copy(ret, data);
    amxc_var_delete(&data);
    return 0;
}
