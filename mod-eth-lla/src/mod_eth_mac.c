/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <net/if.h>
#include <net/if_arp.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_eth_common.h"
#include "mod_eth_mac.h"

#define ME "ethernet"

static int read_mac(const char* name, amxc_string_t* current_mac_str) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ifreq ifr;
    unsigned char* mac = NULL;

    when_str_empty_trace(name, exit, ERROR, "Interface name is empty");
    when_null_trace(current_mac_str, exit, ERROR, "Given current_mac_str pointer is NULL");

    memset(&ifr, 0, sizeof(struct ifreq));
    strncpy(ifr.ifr_name, name, IF_NAMESIZE);
    ifr.ifr_name[IF_NAMESIZE - 1] = '\0';

    if(ioctl(ioctl_fd_get(), SIOCGIFHWADDR, &ifr) < 0) {
        SAH_TRACEZ_ERROR(ME, "%s, failed to get mac address for interface %s", strerror(errno), ifr.ifr_name);
        goto exit;
    }

    mac = (unsigned char*) ifr.ifr_hwaddr.sa_data;
    amxc_string_setf(current_mac_str, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    rv = amxc_string_to_lower(current_mac_str);
    when_failed_trace(rv, exit, ERROR, "Failed to convert current MAC '%s' to lowercase", amxc_string_get(current_mac_str, 0));

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int write_mac(const char* name, const char* mac) {
    SAH_TRACEZ_IN(ME);
    struct ifreq ifr;
    int rv = -1;
    int ret = 0;
    bool was_enabled = false;

    when_str_empty_trace(name, exit, ERROR, "Interface name is empty");
    when_str_empty_trace(mac, exit, ERROR, "Mac address is empty");

    memset(&ifr, 0, sizeof(struct ifreq));
    strncpy(ifr.ifr_name, name, IF_NAMESIZE);
    ifr.ifr_name[IF_NAMESIZE - 1] = '\0';

    when_false(intf_is_ethernet(ifr), exit);

    ret = sscanf(mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &ifr.ifr_hwaddr.sa_data[0],
                 &ifr.ifr_hwaddr.sa_data[1], &ifr.ifr_hwaddr.sa_data[2], &ifr.ifr_hwaddr.sa_data[3],
                 &ifr.ifr_hwaddr.sa_data[4], &ifr.ifr_hwaddr.sa_data[5]);
    when_false_trace(ret == 6, exit, ERROR, "mac conversion failed");

    ifr.ifr_hwaddr.sa_family = ARPHRD_ETHER;
    was_enabled = ioctl_intf_enable(ifr, false);
    if(ioctl(ioctl_fd_get(), SIOCSIFHWADDR, &ifr) < 0) {
        SAH_TRACEZ_ERROR(ME, "%s, failed to set mac address %s for interface %s", strerror(errno), mac, ifr.ifr_name);
        goto exit;
    }

    rv = 0;

exit:
    if(was_enabled) {
        ioctl_intf_enable(ifr, true);
    }

    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * Required data:
 *  - netdev interface
 */
int mac_address_get(UNUSED const char* function_name,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* intf_name = GET_CHAR(args, "intf_name");
    amxc_string_t mac_addr;

    amxc_string_init(&mac_addr, 0);

    when_false_trace(ioctl_fd_get() > 0, exit, ERROR, "no valid file descriptor");
    when_str_empty(intf_name, exit);

    rv = read_mac(intf_name, &mac_addr);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "mac_address", amxc_string_get(&mac_addr, 0));

exit:
    amxc_string_clean(&mac_addr);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * Required data:
 *  - netdev interface
 *  - mac address
 */
int mac_address_set(UNUSED const char* function_name,
                    amxc_var_t* args,
                    UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* netdev_intf = GET_CHAR(args, "intf_name");
    const char* mac_addr = GET_CHAR(args, "mac_address");
    amxc_string_t current_mac_str;
    amxc_string_t requested_mac_str;
    const char* requested_mac = NULL;

    amxc_string_init(&current_mac_str, 0);
    amxc_string_init(&requested_mac_str, 0);

    when_false_trace(ioctl_fd_get() > 0, exit, ERROR, "no valid file descriptor");
    when_str_empty_trace(netdev_intf, exit, ERROR, "Given netdev intf is empty");
    when_str_empty_trace(mac_addr, exit, ERROR, "Given mac address is empty");

    rv = read_mac(netdev_intf, &current_mac_str);
    when_failed_trace(rv, exit, ERROR, "Failed to read mac before setting");

    amxc_string_set(&requested_mac_str, mac_addr);
    rv = amxc_string_to_lower(&requested_mac_str);
    when_failed_trace(rv, exit, ERROR, "Failed to convert requested MAC '%s' to lowercase", amxc_string_get(&requested_mac_str, 0));
    requested_mac = amxc_string_get(&requested_mac_str, 0);

    if(strcmp(requested_mac, amxc_string_get(&current_mac_str, 0)) == 0) {
        SAH_TRACEZ_WARNING(ME, "Interface %s already has MAC address %s, not setting it again",
                           netdev_intf, requested_mac);
        rv = 0;
        goto exit;
    }

    rv = write_mac(netdev_intf, requested_mac);

exit:
    amxc_string_clean(&current_mac_str);
    amxc_string_clean(&requested_mac_str);
    SAH_TRACEZ_OUT(ME);
    return rv;
}