/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <linux/ipv6.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#include "mod_eth_mtu.h"
#define ME "ethernet"

static int file_write(const char* full_path, const int val) {
    SAH_TRACEZ_IN(ME);
    FILE* fd = fopen(full_path, "w");
    int rv = -1;

    when_null_trace(fd, exit, ERROR, "Failed to open file[%s], %s", full_path, strerror(errno));

    if(fprintf(fd, "%d", val) <= 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to write value '%d' to '%s'", val, full_path);
    } else {
        fflush(fd);
        SAH_TRACEZ_INFO(ME, "Value '%d' successfully written to '%s'", val, full_path);
    }

    when_failed_trace(fclose(fd), exit, ERROR, "Failed to close file descriptor: %s", strerror(errno));
    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static long file_read(const char* full_path) {
    SAH_TRACEZ_IN(ME);
    FILE* fd = fopen(full_path, "r");
    char buf[32] = {0};
    long result = -1;
    char* endptr;

    when_null_trace(fd, exit, ERROR, "Failed to open file[%s]", full_path);
    when_null_trace(fgets(buf, sizeof(buf), fd), exit, ERROR, "Failed to read %s", full_path);

    result = strtol(buf, &endptr, 10);

    if(endptr == buf) {
        result = -1;
    }

exit:
    if(fd != NULL) {
        fclose(fd);
        fd = NULL;
    }
    SAH_TRACEZ_OUT(ME);
    return result;
}

static long proc_file_read(const char* intf_name, const char* file_name) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    const char* full_path = NULL;
    long result = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "/proc/sys/net/ipv6/conf/%s/%s", intf_name, file_name);
    full_path = amxc_string_get(&path, 0);

    result = file_read(full_path);

    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return result;
}

static int proc_file_write(const char* intf_name, const char* file_name, const int val) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    const char* full_path = NULL;
    int rv = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "/proc/sys/net/ipv6/conf/%s/%s", intf_name, file_name);
    full_path = amxc_string_get(&path, 0);

    rv = file_write(full_path, val);

    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int sys_file_write(const char* intf_name, const char* file_name, const int val) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    const char* full_path = NULL;
    int rv = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "/sys/class/net/%s/%s", intf_name, file_name);
    full_path = amxc_string_get(&path, 0);

    rv = file_write(full_path, val);

    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mtu_set(UNUSED const char* function_name,
            amxc_var_t* params,
            UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    const char* intf_name = GET_CHAR(params, "intf_name");
    uint32_t mtu = GET_UINT32(params, "mtu_size");
    int rv = -1;
    uint32_t ipv6_path_mtu = 0;
    int accept_ra_mtu = 0;

    when_str_empty_trace(intf_name, exit, ERROR, "No interface name provided");
    when_null_trace(GET_ARG(params, "mtu_size"), exit, ERROR, "No mtu size provided");

    ipv6_path_mtu = proc_file_read(intf_name, "mtu");
    accept_ra_mtu = proc_file_read(intf_name, "accept_ra_mtu");

    if(mtu < IPV6_MIN_MTU) {
        SAH_TRACEZ_WARNING(ME, "New MTU is lower than minimum IPv6 MTU of %d," \
                           "you might lose IPv6 connectivity on interface %s", IPV6_MIN_MTU, intf_name);
    }

    rv = sys_file_write(intf_name, "mtu", mtu);

    /*
     * Writing to sysfs also changes the IPv6 MTU setting in procfs.
     * If the IPv6 MTU was configured by router advertisements,
     * write the original value back to procfs if it's smaller than the new MTU
     * and larger than the minimum IPv6 MTU.
     */
    if((accept_ra_mtu == 1) && (ipv6_path_mtu < mtu) && (mtu >= IPV6_MIN_MTU)) {
        proc_file_write(intf_name, "mtu", ipv6_path_mtu);
    }

    when_failed_trace(rv, exit, ERROR, "Failed to set MTU of interface %s to %d", intf_name, mtu);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
